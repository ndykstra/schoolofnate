﻿namespace LessonOne
{
    /// <summary>
    /// Represents a person.
    /// </summary>
    public class Person
    {
        /// <summary>
        /// Gets or sets the first name of this person.
        /// </summary>
        public string FirstName { get; set; }

        /// <summary>
        /// Gets or sets the last name of this person.
        /// </summary>
        public string LastName { get; set; }

        /// <summary>
        /// Gets or sets the age of this person.
        /// </summary>
        public int Age { get; set; }

        /// <summary>
        /// Instantiates a new person.
        /// </summary>
        /// <param name="firstName">Their first name.</param>
        /// <param name="lastName">Their last name.</param>
        /// <param name="age">Their age. Optional, default 1.</param>
        public Person(string firstName, string lastName, int age = 1)
        {
            FirstName = firstName;
            LastName = lastName;
            Age = age;
        }

        /// <summary>
        /// Gets a string describing this person.
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return $"{FirstName} {LastName} is {Age} years old.";
        }
    }
}
