﻿using System;

namespace LessonOne
{
    public class Ellipse : IShape
    {
        public double MinorAxis { get; set; }
        public double MajorAxis { get; set; }

        public double Area => Math.PI * MajorAxis * MinorAxis;

        public ShapeType GetShape()
        {
            return ShapeType.Ellipse;
        }

        public Ellipse(double majorAxis, double minorAxis)
        {
            this.MajorAxis = majorAxis;
            this.MinorAxis = minorAxis;
        }
    }
}
