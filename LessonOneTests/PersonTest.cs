﻿using LessonOne;
using NUnit.Framework;

namespace LessonOneTests
{
    [TestFixture]
    public class PersonTests
    {
        [TestCase("Nate", "Dykstra")]
        [TestCase("Siri", "Jaikongla")]
        [TestCase("Lana", "Dykstra")]
        public void TestConstructor(string firstName, string lastName)
        {
            var person = new Person(firstName, lastName);

            Assert.IsNotNull(person);
        }

        [TestCase("Lana", "Dykstra", ExpectedResult = 1)]
        public int TestDefaultAge(string firstName, string lastName)
        {
            var person = new Person(firstName, lastName);
            return person.Age;
        }

        [TestCase("Nate", "Dykstra", 32, ExpectedResult = 32)]
        [TestCase("Siri", "Jaikongla", 38, ExpectedResult = 38)]
        public int TestGetAge(string firstName, string lastName, int age)
        {
            var person = new Person(firstName, lastName, age);
            return person.Age;
        }

        [Test]
        public void TestFirstName()
        {
            var siri = new Person("Siri", "Jaikongla");

            Assert.AreEqual("Siri", siri.FirstName);
        }

        [Test]
        public void TestLastName()
        {
            var nate = new Person("Nate", "Dykstra");

            Assert.AreEqual("Dykstra", nate.LastName);
        }
    }
}