﻿namespace LessonOne
{
    /// <summary>
    /// Represents a square shape.
    /// </summary>
    public class Square : Rectangle
    {
        public Square(double side) : base(side, side)
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public override ShapeType GetShape()
        {
            return ShapeType.Square;
        }
    }
}