﻿using NUnit.Framework;
using LessonOne;

namespace LessonOneTests
{
    [TestFixture]
    public class CircleTests
    {
        [Test]
        public void TestArea()
        {
            var area = new Circle(2);

            Assert.IsNotNull(area);
        }

        [Test]
        public void TestShape()
        {
            var circle = new Circle(6.5);

            Assert.AreEqual(ShapeType.Circle, circle.GetShape());
        }

        [TestCase(2)]
        [TestCase(10)]
        [TestCase(14)]
        [TestCase(20)]
        public void TestConstructor(int diameter)
        {
            var area = new Circle(diameter);

            Assert.IsNotNull(area);
        }

        [TestCase(2, 3.14, .01)]
        [TestCase(7.5, 44.18, .01)]
        [TestCase(115.2, 10423.05, .01)]
        public void TestDiameterResult(double diameter, double area, double delta)
        {
            var circle = new Circle(diameter);

            Assert.AreEqual(area, circle.Area, delta);
        }
    }
}
