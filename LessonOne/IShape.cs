﻿namespace LessonOne
{
    /// <summary>
    /// The types of shapes supported.
    /// </summary>
    public enum ShapeType
    {
        Rectangle,
        Circle,
        Square,
        Ellipse
    }

    /// <summary>
    /// The interface all 2 dimensional shapes should inherit.
    /// </summary>
    public interface IShape
    {
        /// <summary>
        /// Gets the area of the 2D shape.
        /// </summary>
        double Area { get; }

        /// <summary>
        /// Gets the <see cref="ShapeType"/> of this shape.
        /// </summary>
        /// <returns></returns>
        ShapeType GetShape();
    }
}
