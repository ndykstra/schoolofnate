﻿using System;

namespace LessonOne
{
    /// <summary>
    /// Represents a circle.
    /// </summary>
    public class Circle : Ellipse
    {
        /// <summary>
        /// Gets or sets the diameter of the circle.
        /// </summary>
        public double Diameter { get; set; }

        /// <summary>
        /// Instantiates a new circle with the specified diameter.
        /// </summary>
        /// <param name="diameter"></param>
        public Circle(double diameter) : base(diameter/2, diameter/2)
        {
            this.Diameter = diameter;
        }

        /// <summary>
        /// Gets the <see cref="ShapeType"/>
        /// </summary>
        /// <returns></returns>
        public new ShapeType GetShape()
        {
            return ShapeType.Circle;
        }
    }
}