﻿namespace LessonOne
{
    /// <summary>
    /// Represents a rectangle.
    /// </summary>
    public class Rectangle : IShape
    {
        /// <summary>
        /// Gets or sets the height of this shape.
        /// </summary>
        public double Height { get; set; }

        /// <summary>
        /// Gets or sets the width of this shape.
        /// </summary>
        public double Width { get; set; }

        /// <summary>
        /// Gets the area of this shape.
        /// </summary>
        public double Area => this.Height * this.Width;

        /// <summary>
        /// Instantiates a new instance of the rectangle.
        /// </summary>
        /// <param name="height">The height of this rectangle.</param>
        /// <param name="width">The width of this rectangle.</param>
        public Rectangle(double height, double width)
        {
            this.Height = height;
            this.Width = width;
        }

        /// <summary>
        /// Gets the <see cref="ShapeType"/>
        /// </summary>
        /// <returns></returns>
        public virtual ShapeType GetShape()
        {
            return ShapeType.Rectangle;
        }
    }
}